/*****************************************************************************
 * MediaLibraryKit
 *****************************************************************************
 * Copyright (C) 2013 Felix Paul Kühne
 *
 * Authors: Felix Paul Kühne <fkuehne # videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#import <MediaLibraryKit/MLAlbum.h>
#import <MediaLibraryKit/MLAlbumTrack.h>
#import <MediaLibraryKit/MLArtist.h>
#import <MediaLibraryKit/MLAudioTrack.h>
#import <MediaLibraryKit/MLFile.h>
#import <MediaLibraryKit/MLFolder.h>
#import <MediaLibraryKit/MLGenre.h>
#import <MediaLibraryKit/MLHistoryEntry.h>
#import <MediaLibraryKit/MLLabel.h>
#import <MediaLibraryKit/MLMedia.h>
#import <MediaLibraryKit/MLMediaLibrary.h>
#import <MediaLibraryKit/MLMediaMetadata.h>
#import <MediaLibraryKit/MLMovie.h>
#import <MediaLibraryKit/MLPlaylist.h>
#import <MediaLibraryKit/MLShow.h>
#import <MediaLibraryKit/MLShowEpisode.h>
#import <MediaLibraryKit/MLVideoTrack.h>


@class MLAlbum;
@class MLAlbumTrack;
@class MLArtist;
@class MLAudioTrack;
@class MLFile;
@class MLFolder;
@class MLGenre;
@class MLHistoryEntry;
@class MLLabel;
@class MLMedia;
@class MLMediaLibrary;
@class MLMediaMetadata;
@class MLMovie;
@class MLPlaylist;
@class MLShow;
@class MLShowEpisode;
@class MLVideoTrack;
